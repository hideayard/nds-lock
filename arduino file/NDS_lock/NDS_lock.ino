/* NDS Lock by hideayard@gmail.com */
#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiClientSecureBearSSL.h>

#include <WiFiClient.h>
#include <Servo.h>

const uint8_t fingerprint[20] = {0x17,0x77,0xA3,0x11,0x26,0xCC,0xF6,0x9D,0x6B,0x64,0x96,0x19,0x79,0x31,0x04,0x85,0xB7,0x61,0x43,0xD9};

Servo servo;

#ifndef STASSID
#define STASSID "Maxi"
#define STAPSK  "dilarangkonek"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);
ESP8266WiFiMulti WiFiMulti;

String nodeIP = "";
const int led = 13;
String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

const int buttonPin = 5,BackServoPin = 16,RelayPin = 14;     // the number of the pushbutton pin
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
int ledState = HIGH;         // the current state of the output pin

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}
void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(BackServoPin, OUTPUT);
  pinMode(RelayPin, OUTPUT);
  digitalWrite(led, ledState);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

// Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  nodeIP = WiFi.localIP().toString();
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(nodeIP);

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  
  server.on("/open", []() {
    digitalWrite(BackServoPin, LOW);
    Serial.println("Opening Door via web");
    server.send(200, "text/plain", "Open Door");
    open();
    inputString = "get";
  });

  server.on("/close", []() {
    digitalWrite(BackServoPin, HIGH);
    Serial.println("Closing Door via web");
    server.send(200, "text/plain", "Close Door");
    close();
  });
//  
  server.on("/ONRELAY", []() {
    digitalWrite(RelayPin, LOW);//start on low (low=on)
    Serial.println("ON Relay via web");
    server.send(200, "text/plain", "Relay HIGH");
    close();
  });
//  
  server.on("/OFFRELAY", []() {
    digitalWrite(RelayPin, HIGH);//stop on HIGH (HIGH=off)
    Serial.println("OFF Relay via web");
    server.send(200, "text/plain", "Relay LOW");
    close();
  });  
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
    inputString.reserve(200);
  Serial.println("HTTP client started");
https_client();
Serial.println("HTTP client ended");

}

void loop() {
    int reading = digitalRead(buttonPin);
    if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:
    
    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        Serial.println("Button Click");
        open();
        ledState = !ledState;
      }
    }
  }
  // set the LED:
  digitalWrite(led, ledState);
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
///----- end check button ------

  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.println(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  // wait for WiFi connection
//  if ((WiFiMulti.run() == WL_CONNECTED) && (inputString == "get"))
//  {
//      https_client();
//  }
    server.handleClient();
    MDNS.update();
//  delay(10000);
}


void open()
{
//  Serial.println("opening door");
  digitalWrite(BackServoPin, HIGH);
  moveTo(180,15);
  delay(2000);
  digitalWrite(BackServoPin, LOW);
}

void close()
{
//  Serial.println("closing door");
  digitalWrite(BackServoPin, LOW);
  moveTo(0,15);
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

int pos1=0,pos=0;
void moveTo(int position, int speed)
{
  servo.attach(2);  //D4
  int mapSpeed = map(speed,0,30,30,0);
  if(position>pos)
  {
    for(pos=pos1;pos<=position;pos+=1)
    {
      servo.write(pos);
      pos1=pos;
      delay(mapSpeed);
    }
  }
  else
  {
    for(pos=pos1;pos>=position;pos-=1)
    {
      servo.write(pos);
      pos1=pos;
      delay(mapSpeed);
    }
  }
  servo.detach();
}

void https_client()
{
    //--------------
std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

    client->setFingerprint(fingerprint);
    HTTPClient https;
    String postData = "loc=SEMARANG(C16)&ip=http://"+nodeIP ,serial_data = "";
    Serial.print("[HTTPS] begin to https://nexwaveindonesia.com/lock/update-node \n");
    https_push_log("[HTTPS] begin to https://nexwaveindonesia.com/lock/update-node \n");
    if (https.begin(*client, "https://nexwaveindonesia.com/lock/update-node")) {  // HTTPS
      https.addHeader("Content-Type", "application/x-www-form-urlencoded");
      Serial.print("[HTTPS] POST Data : ");
      https_push_log("[HTTPS] POST Data : ");
      Serial.println(postData);
      // start connection and send HTTP header
      int httpCode = https.POST(postData);
      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
        serial_data = "[HTTPS] GET... code: "+httpCode;
        https_push_log(serial_data);
        String payload = https.getString();
        https_push_log(payload);
        Serial.println(payload);
        // file found at server
//        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
//          String payload = https.getString();
//          Serial.println(payload);
//        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
        serial_data = "[HTTPS] GET... failed, error: "+ https.errorToString(httpCode);
        https_push_log(serial_data);

      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
    //==============
}

void https_push_log(String info)
{
    //--------------
std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

    client->setFingerprint(fingerprint);
    HTTPClient https;
    String postData = "loc=SEMARANG(C16)&source=nodemcu&username=PTNW1234&node_action=SERIAL_LOG&info="+info ;
    if (https.begin(*client, "https://nexwaveindonesia.com/lock/push-log")) {  // HTTPS
      https.addHeader("Content-Type", "application/x-www-form-urlencoded");
      Serial.print("[HTTPS] POST Data : ");
      Serial.println(postData);
      // start connection and send HTTP header
      int httpCode = https.POST(postData);
      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
        String payload = https.getString();
        Serial.println(payload);
        // file found at server
//        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
//          String payload = https.getString();
//          Serial.println(payload);
//        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
    //==============
}
